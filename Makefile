SOURCE_FILES=$(wildcard *.cpp)
OBJ_FILES=$(patsubst %.o, %.cpp, $(SOURCE_FILES))
EXECUTABLE=project.exe
CC=g++

$(EXECUTABLE) : $(OBJ_FILES)
	$(CC) $^ -o $@

%.o : %.cpp
	$(CC) -c $^ -o $@

clear:
	rm -rf *.o
