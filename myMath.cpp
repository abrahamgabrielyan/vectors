#include "myMath.h"

double getModuleOfVector(int* vector)
{
	int arrSize = sizeof(vector)/sizeof(int);

	constexpr int vectorXIndex = 0;
    constexpr int vectorYIndex = 1;

	//if vector exists in 3d space
	if(arrSize == 3)
	{
		constexpr int vectorZIndex = 2;
	
		return sqrt(((pow(vector[vectorXIndex], 2)) + (pow(vector[vectorYIndex], 2)) + (pow(vector[vectorZIndex], 2))));
	}
	//else if vector exists in 2d space
	else
	{
		return sqrt(((pow(vector[vectorXIndex], 2)) + (pow(vector[vectorYIndex], 2))));	
	}
}

double getCosinusOfAngle(int* vector1, int* vector2)
{
	constexpr int vectorXIndex = 0;
    constexpr int vectorYIndex = 1;

	if(vector1[vectorXIndex] == 0 && vector1[vectorYIndex] == 0 || vector2[vectorXIndex] == 0 && vector2[vectorYIndex] == 0)
	{
		std::cout << "Can't calculate. One of the vectors is a null vector" << std::endl;
		exit(1);
	}
	else
	{
		int top = vector1[vectorXIndex] * vector2[vectorXIndex] + vector1[vectorYIndex] * vector2[vectorYIndex];
	
		double bot = sqrt((pow(vector1[vectorXIndex], 2) + pow(vector1[vectorYIndex], 2))) * sqrt((pow(vector2[vectorXIndex], 2) + pow(vector2[vectorYIndex], 2)));
	
		double cosinus = top / bot;

		return cosinus;
	}	
}

void crossProductOfVectors(int* vector1, int* vector2, int* vectorGot)
{
	constexpr int vectorXIndex = 0;
	constexpr int vectorYIndex = 1;
	constexpr int vectorZIndex = 2;

	vectorGot[vectorXIndex] = vector1[vectorYIndex] * vector2[vectorZIndex] - vector1[vectorZIndex] * vector2[vectorYIndex];
	vectorGot[vectorYIndex] = vector1[vectorXIndex] * vector2[vectorZIndex] - vector1[vectorZIndex] * vector2[vectorXIndex];
	vectorGot[vectorZIndex] = vector1[vectorXIndex] * vector2[vectorYIndex] - vector1[vectorYIndex] * vector2[vectorXIndex];
}

double dotProductOfVectors(int* vector1, int* vector2)
{
	double answer = getModuleOfVector(vector1) * getModuleOfVector(vector2) * getCosinusOfAngle(vector1, vector2);

	return answer;
}

double intersectionAngle(int* vector1, int* vector2)
{
	constexpr int vectorXIndex = 0;
	constexpr int vectorYIndex = 1;
	constexpr int vectorZIndex = 2;

	constexpr double toGetAngleFromRadian = 180.0/3.141592653589793238463;

	int top = abs(vector1[vectorXIndex] * vector2[vectorXIndex] + vector1[vectorYIndex] * vector2[vectorYIndex] + vector1[vectorZIndex] * vector2[vectorZIndex]);

    double bot = sqrt(pow(vector1[vectorXIndex], 2) + pow(vector1[vectorYIndex], 2) + pow(vector1[vectorZIndex], 2)) * sqrt(pow(vector2[vectorXIndex], 2) + pow(vector2[vectorYIndex], 2) + pow(vector2[vectorZIndex], 2));

	double cosinusOfAngle = top / bot;

	return acos(cosinusOfAngle) * toGetAngleFromRadian;
}
