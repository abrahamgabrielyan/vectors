#pragma once

#include <cmath>
#include <iostream>

double getModuleOfVector(int* vector);

double getCosinusOfAngle(int* vector1, int* vector2);

void crossProductOfVectors(int* vector1, int* vector2, int* vectorGot);

double dotProductOfVectors(int* vector1, int* vector2);

double intersectionAngle(int* vector1, int* vector2);
